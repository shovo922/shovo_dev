(function ($) {
    "use strict";


     // @right sidebar Accordian
        $(document).ready(function(){
         $(".card-header").click(function()
        {   

        if($(this).next(".card-body").hasClass("active")){
            $(this).next(".card-body").removeClass("active").slideUp()
            $(this).children("span").removeClass("fa-minus").addClass("fa-plus") 
        }

        else{
            $(".card .card-body").slideUp()
            $(".card .card-header span").removeClass("fa-minus").addClass("fa-plus")
            $(this).next(".card-body").addClass("active").slideDown()
            $(this).children("span").removeClass("fa-plus").addClass("fa-minus")
        }

        })
        })
   //  @right sidebar Accordian end


   
   // @sticky nav end


//  @loader Animation
    jQuery(window).load(function() {
    jQuery("#loading").fadeOut(500);
    });


    // scroll up 
    $.scrollUp({
      scrollName: 'up', // Element ID
      topDistance: '300', // Distance from top before showing element (px)
      topSpeed: 300, // Speed back to top (ms)
      animation: 'fade', // Fade, slide, none
      animationInSpeed: 200, // Animation in speed (ms)
      animationOutSpeed: 200, // Animation out speed (ms)
      scrollText: '<i class="fa fa-angle-up" aria-hidden="true"></i>', // Text for element
      activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });

    // main menu show hide 

    //for menu active class
$('.kcsl-mianMenu-cross').on('click', function(event) {
	//$(this).siblings('.active').removeClass('active');
	$('.kcsl-main-menu-box').addClass('active');
	event.preventDefault();
});

$('.btn-corss-mianMenu').on('click', function(event) {
	//$(this).siblings('.active').removeClass('active');
	$('.kcsl-main-menu-box').removeClass('active');
	event.preventDefault();
});

$('#menu-small-screen .kcsl-mianMenu-cross').on('click', function(event) {

  $('.kcsl-main-menu-box').addClass('mobActive');
  $('.kcsl-main-menu-box').removeClass('active');
  //alert('hello data');
	event.preventDefault();
});



$('.kcsl-mianMenu-crossMob').on('click', function(event) {
	$('.kcsl-main-menu-boxMobile').addClass('activeMob');
  //$('.kcsl-main-menu-box').removeClass('mobActive');
  //alert('hello world');
	event.preventDefault();
});
$('.btn-corss-mianMenuMob').on('click', function(event) {
	//$('.kcsl-main-menu-boxMobile').addClass('activeMob');
  $('.kcsl-main-menu-boxMobile').removeClass('activeMob');
  //alert('hello world');
	event.preventDefault();
});





 

// $( window ).resize(function() {
//   if($(window).width() < 1000) { 
//     $(".kcsl-main-menu-box").addClass("mobiClass"); 
//     //$("body").removeClass("myClass");   
//   }  
// });

// home slider

$('.home-slider-active').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

// @ slider for topics 
$('.active-kcsl-topics').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});




})(jQuery); 

